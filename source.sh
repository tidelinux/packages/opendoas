# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://github.com/Duncaen/OpenDoas/releases/download/v${BPM_PKG_VERSION}/opendoas-${BPM_PKG_VERSION}.tar.xz"
FILENAME="${DOWNLOAD##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  wget "$DOWNLOAD"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
}

# The build function is executed in the source directory
# This function is used to compile the source code
build() {
  ./configure --prefix=/usr --with-timestamp
  make
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  make DESTDIR="$BPM_OUTPUT" install
  mkdir -p "$BPM_OUTPUT"/etc
  cp ../doas.conf "$BPM_OUTPUT"/etc/doas.conf
  chmod -c 0400 "$BPM_OUTPUT"/etc/doas.conf
  cp ../doas-pam "$BPM_OUTPUT"/etc/pam.d/doas
  mkdir -p "$BPM_OUTPUT"/usr/share/licenses/opendoas
  cp LICENSE "$BPM_OUTPUT"/usr/share/licenses/opendoas/LICENSE
}
